# TruckFitAdmin

#Git Guidelines

git usage

first make sure that you have added `upstream` to your remotes (you only need to do this once not every time)

```
git remote add upstream git@github.com:pythagorasllc/TruckFitAdmin.git
```

Now every time that you start a new task you should be doing the following:

1. first you need to go to your dev branch and make sure you have the latest that matches upstream

```
git checkout dev
git fetch upstream
git reset --hard upstream/dev
```

2. create a new branch for your feature. Replace branch_name with a name appropriate to your task prefix by jira task number for examle the task https://pythagoras.atlassian.net/browse/TRUCKFIT-94 could have a branch name`94_users_login`.

```
git checkout -b branch_name
```

3. When you are done with your task get the latest from upstream to make sure you dont have any conflicts before opening the PR:

```
git fetch upstream
git rebase upstream/dev
```

4. Create a PR on upstream/dev.
