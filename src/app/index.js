import React from 'react';
import config from 'config';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, RouterContext} from 'react-router';
import {hashHistory} from 'react-router';
import getRoutes from './routes';
import configureStore from './state/store/configureStore';
import last from 'lodash/last';
import '../styles/_index';

// FIXME: we should just have initial state in each reducer instead of a single initialState object
const store = configureStore({});
const routes = getRoutes(store);

/**
 * RouterWrapper gets called every time a route changes. It checks if the component has a onRoute static
 * method and calls it passing in dispatch getStore and so on. The onRoute method can be used for loading
 * data for your page instead of having to do componentWillMount in combination with componentWillReceiveProps
 * and checking for if the route has changed.
 */
class RouteWrapper extends React.Component {
	componentWillMount(){
		this.callOnRoute(this.props);
	}

	componentWillReceiveProps(nextProps){
		// if (nextProps.location.pathname !== this.props.location.pathname) {
		// 	this.callOnRoute(nextProps, this.props);
		// }

		// changed it to alawyas trigger the onRoute even when clicking a link to the current url:
		this.callOnRoute(nextProps, this.props);
	}

	callOnRoute(props, previousProps){
		const {dispatch, getState} = store;
		const route = last(props.routes);
		const component = route.component;

		let previous = {};

		if(previousProps){
			const prevRoute = last(previousProps.routes);
			previous.location= previousProps.location;
			previous.page= prevRoute.page;
		}

		dispatch({
			type: 'ROUTE',
			payload: {
				prevLocation: previous.location,
				prevPage: previous.page,
				location: props.location,
				page: route.page
			}
		});

		if (component.onRoute) {
			component.onRoute({...this.props, dispatch: dispatch, getState});
		}
	}

	render() {
		return (
			<RouterContext {...this.props} />
		);
	}
}

const init = () => {
	render(
		<Provider store={store}>
			<Router history={hashHistory} routes={routes} render={(props) => <RouteWrapper {...props} />}/>
		</Provider>,
		document.getElementById('root')
	);

	window.handleOpenUrl = (url) => {

		hashHistory.push(url.replace('truckfit://', ''));
		// or for a full page refresh:
		// window.location = window.location.href.replace(/#.*$/, '') + '#' + (url.replace('truckfit://', ''));

		// Old code, I don't think this will work ----
		// store.dispatch({
		// 	type: 'ROUTE',
		// 	payload: {
		// 		prevLocation: '',
		// 		prevPage: '',
		// 		location: '',
		// 		page: url.replace('truckfit://', '')
		// 	}
		// });
	};

	setInterval(()=>{
		const state = store.getState();
		if (state.auth.token) {
			store.dispatch({
				type: 'UPDATE_INTERVAL',
				meta: {
					source: 'interval'
				}
			});
		}

	}, config.updateInterval);
};

// TODO: use something like npm package `debug`
// that is easily toggled on and off with environment flags
console.info(process.env.NODE_ENV, process.env.SERVER_ENV, process.env.__DEV__);

if (process.env.NODE_ENV === 'production' && process.env.SERVER_ENV === 'production' && !process.env.__DEV__) {
	document.addEventListener('deviceready', init, false);
}
else {
	init();
}
