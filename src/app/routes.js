import React from 'react';
import {Route, IndexRoute, Redirect} from 'react-router';
import Auth from './libs/Auth';

// Import Views
import App from './AppView';
import DefaultLayout from './layouts/default/DefaultLayoutView';
import LoaderLayout from './layouts/loader/LoaderLayoutView';
import Login from './modules/login/LoginView';
import Home from './modules/home/HomeView';
import user from './modules/home/userView';
import NotFoundView from './modules/notFound/NotFoundView';
import ForbiddenView from './modules/forbidden/ForbiddenView';
import last from 'lodash/last';

export default function getRoutes(store) {
	/**
	* You can set authorize = true as a static in the component or alternatively add authorize property on the route
	*/
	function onRoute({nextState, replace, callback}){
		const { dispatch, getState } = store;
		const currentUrl = nextState.location.pathname + nextState.location.search + nextState.location.hash;
		const route = last(nextState.routes);
		const authorize1 = route.component.authorize || route.authorize;
		if(authorize1){
			if(!Auth.isAuthorized({dispatch: dispatch, permission: authorize1})){
				replace('/home?redirectUrl=' + encodeURIComponent(currentUrl));
			}
		}
		callback();
	}

	function onEnter(nextState, replace, callback){
		return onRoute({nextState, replace, callback});
	}

	function onChange(prevState, nextState, replace, callback){
		return onRoute({prevState, nextState, replace, callback});
	}

	return (
		<Route path="/" onEnter={onEnter} onChange={onChange} component={App}>
			<Route component={DefaultLayout}>
				<Route component={LoaderLayout}>

					<IndexRoute component={Home} authorize={['truckfitadmin']} authorize1 page="home"/>
				</Route>
			</Route>
			<Route component={DefaultLayout}>
				<Route path="/login" component={Login} />
				<Route path="/user" component={user} />
				<Route path="/home" component={Home} />
				<Route path="404" component={NotFoundView} page="notFound"/>
				<Route path="/forbidden" component={ForbiddenView} page="forbidden"/>
				<Redirect from="*" to="404"/>
			</Route>
		</Route>
	);
}
