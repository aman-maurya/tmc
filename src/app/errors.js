// HTTP errors
export const HttpNotFoundError = createError('HttpNotFoundError', '404 Not Found');
export const HttpNotAuthorizedError = createError('HttpNotAuthorizedError', '401 Not Authorized');
export const HttpForbiddenError = createError('HttpForbiddenError', '403 Forbidden');
export const HttpServerError = createError('HttpApiError', '500 Internal Server Error');

// Auth Errors
export const NotAuthorizedError = createError('NotAuthorizedError', 'Not Authorized');
export const ForbiddenError = createError('ForbiddenError', 'Forbidden');

function createError(name, defaultMessage){
	var customError = function(message){
		this.name = name;
		this.message = message || defaultMessage;
		this.stack = (new Error()).stack;
		// if(Error.captureStackTrace){
		// 	// for v8 this is preferable:
		// 	Error.captureStackTrace(this, customError);
		// }else{
		// 	this.stack = (new Error()).stack;
		// }
	};
	customError.prototype = Object.create(Error.prototype);
	customError.prototype.constructor = customError;

	return customError;
}
