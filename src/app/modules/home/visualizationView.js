import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Spinner from '../../components/Spinner';
import { AuthorizedComponent } from 'react-router-role-authorization';
class visualizationView extends AuthorizedComponent {
	constructor(props) {
	    super(props);
	    this.userRoles = [props.auth.role];
	    this.notAuthorizedPath = '/login';
	}
	render() {
		return (
			<div className="container visualization-page">
				<div className="row company-dashboard-container">
					<iframe className="company-dashboard" src="http://localhost:5601/app/kibana#/dashboard/12a50b10-0661-11e7-807b-5930bd6dbdea?embed=true&_g=(refreshInterval%3A(display%3AOff%2Cpause%3A!f%2Cvalue%3A0)%2Ctime%3A(from%3Anow-90d%2Cmode%3Aquick%2Cto%3Anow))" height="600" width="800"></iframe>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state, ownProps) {

	return {
		...state
	}
}

export default connect(mapStateToProps)(visualizationView);


