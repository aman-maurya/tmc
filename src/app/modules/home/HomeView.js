import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Spinner from '../../components/Spinner';
import { AuthorizedComponent } from 'react-router-role-authorization';
class HomeView extends Component {
	constructor(props) {
	    super(props);
	    // this.userRoles = [props.auth.role];
	    // this.notAuthorizedPath = '/login';
	}
	render() {
		return (<div className="home-page">

    <div className="container">
        <div className="row">
            <div className="col-sm-3 col-md-3">
                <div className="panel-group">
                    <div className="col-md-12"></div>
                </div>
                <div className="panel-group" id="accordion">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" className='group-list'>Annual Pay Reviwes</a><span className="glyphicon glyphicon-menu-down gly-rotate-90">
                            </span>
                            </h4>
                        </div>
                        <div id="collapseOne" className="panel-collapse collapse in">
                            <div className="panel-body panel-body-border1">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td>
                                                <div className="user two"></div>
                                                <a href="">Dough</a>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user five"></div>
                                                <a href="">Steave</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user three"></div>
                                                <a href="">Justin</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user four"></div>
                                                <a href="">Joe</a>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" className='group-list'>Annual Pay Reviwes</a><span className="glyphicon glyphicon-menu-down  ">
                            </span>
                            </h4>
                        </div>
                        <div id="collapse2" className="panel-collapse collapse ">
                            <div className="panel-body panel-body-border2 ">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td>
                                                <div className="user two"></div>
                                                <a href="">Dough</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user five"></div>
                                                <a href="">Steave</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user three"></div>
                                                <a href="">Justin</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user four"></div>
                                                <a href="">Joe</a>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" className='group-list'>Talent Scouting</a><span className="glyphicon glyphicon-menu-down ">
                            </span>
                            </h4>
                        </div>
                        <div id="collapse3" className="panel-collapse collapse ">
                            <div className="panel-body panel-body-border3">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td>
                                                <div className="user two"></div>
                                                <a href="">Dough</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user five"></div>
                                                <a href="">Steave</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user three"></div>
                                                <a href="">Justin</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user four"></div>
                                                <a href="">Joe</a>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" className='group-list'>Employee Reward Renewal</a><span className="glyphicon glyphicon-menu-down ">
                            </span>
                            </h4>
                        </div>
                        <div id="collapse4" className="panel-collapse collapse ">
                            <div className="panel-body panel-body-border4">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td>
                                                <div className="user two"></div>
                                                <a href="">Dough</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user five"></div>
                                                <a href="">Steave</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user three"></div>
                                                <a href="">Justin</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user four"></div>
                                                <a href="">Joe</a>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" className='group-list'>HR Policies Reviwes</a><span className="glyphicon glyphicon-menu-down ">
                            </span>
                            </h4>
                        </div>
                        <div id="collapse5" className="panel-collapse collapse ">
                            <div className="panel-body panel-body-border5">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td>
                                                <div className="user two"></div>
                                                <a href="">Dough</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user five"></div>
                                                <a href="">Steave</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user three"></div>
                                                <a href="">Justin</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user four"></div>
                                                <a href="">Joe</a>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" className='group-list'>Performance Reviwes</a><span className="glyphicon glyphicon-menu-down ">
                            </span>
                            </h4>
                        </div>
                        <div id="collapse6" className="panel-collapse collapse ">
                            <div className="panel-body panel-body-border6">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td>
                                                <div className="user two"></div>
                                                <a href="">Dough</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user five"></div>
                                                <a href="">Steave</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user three"></div>
                                                <a href="">Justin</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div className="user four"></div>
                                                <a href="">Joe</a>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className="col-sm-9 col-md-9">
                <div className="panel-group">
                    <table className="thead-table">
                        <thead>
                            <tr>
                                <td className="table-head">Jan</td>
                                <td className="table-head">Feb</td>
                                <td className="table-head">March</td>
                            </tr>
                        </thead>
                    </table>

                </div>
                <div className="panel-group" id="accordion">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <table className="table s-men">
                                <thead>
                                    <tr>
                                        <td>
                                            <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-head">115hrs/week</a>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="collapseOnep" className="panel-collapse collapse in">
                            <div className="panel-body">
                                <table className="table s-men">
                                    <thead>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar2">110hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar3">50hrs/Week </a>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <table className="table s-men">
                                <thead>
                                    <tr>
                                        <td>
                                            <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar-head">115hrs/week</a>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="collapse2p" className="panel-collapse collapse ">
                            <div className="panel-body">
                                <table className="table s-men">
                                    <thead>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar2">110hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar3">50hrs/Week </a>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <table className="table s-men">
                                <thead>
                                    <tr>
                                        <td>
                                            <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-head">115hrs/week</a>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="collapse3p" className="panel-collapse collapse ">
                            <div className="panel-body">
                                <table className="table s-men">
                                    <thead>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar2">110hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar3">50hrs/Week </a>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <table className="table s-men">
                                <thead>
                                    <tr>
                                        <td>
                                            <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar-head">115hrs/week</a>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="collapse4p" className="panel-collapse collapse ">
                            <div className="panel-body">
                                <table className="table s-men">
                                    <thead>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar2">110hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar3">50hrs/Week </a>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <table className="table s-men">
                                <thead>
                                    <tr>
                                        <td>
                                            <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-pink bar-head">115hrs/week</a>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="collapse5p" className="panel-collapse collapse ">
                            <div className="panel-body">
                                <table className="table s-men">
                                    <thead>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-pink bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-pink bar2">110hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-pink bar3">50hrs/Week </a>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-pink bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <table className="table s-men">
                                <thead>
                                    <tr>
                                        <td>
                                            <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar-head">115hrs/week</a>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="collapse6p" className="panel-collapse collapse ">
                            <div className="panel-body">
                                <table className="table s-men">
                                    <thead>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar2">110hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar3">50hrs/Week </a>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar1">115hrs/Week </a>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>);
	}
}

function mapStateToProps(state, ownProps) {

	return {
		...state
	}
}

export default connect(mapStateToProps)(HomeView);

