import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Form, FormGroup, FormError, Input,Select} from '../../components/forms/Forms';
import Spinner from '../../components/Spinner';
import { Link } from 'react-router';
import { AuthorizedComponent } from 'react-router-role-authorization';
class userView extends Component {
	constructor(props) {
		super(props);

	}



	render() {

		return (
			<div className="login-view container ">
				 <div className="col-sm-9 col-md-9" >
            <div className="panel-group" >
                <table className="thead-table" ><thead>
                    <tr>
                        <td className="table-head">Jan</td>
                        <td className="table-head">Feb</td>
                        <td className="table-head">March</td>
                    </tr>
                </thead></table>
            </div>
            <div className="panel-group" id="accordion">
                <div className="panel panel-default">
                    <div className="panel-body">
                        <table className="table s-men"><thead>
                            <tr>
                                <td>
                                    <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-head">115hrs/week</a>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </thead></table>
                    </div>
                    <div id="collapseOnep" className="panel-collapse collapse in">
                        <div className="panel-body">
                            <table className="table s-men"><thead>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar2">110hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar3">50hrs/Week </a>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </thead></table>
                        </div>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <table className="table s-men"><thead>
                            <tr>
                                <td>
                                    <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-sky bar-head">115hrs/week</a>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </thead></table>
                    </div>
                    <div id="collapse2p" className="panel-collapse collapse ">
                        <div className="panel-body">
                            <table className="table s-men"><thead>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar2">110hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-sky bar3">50hrs/Week </a>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-sky bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </thead></table>
                        </div>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <table className="table s-men"><thead>
                            <tr>
                                <td>
                                    <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-head">115hrs/week</a>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </thead></table>
                    </div>
                    <div id="collapse3p" className="panel-collapse collapse ">
                        <div className="panel-body">
                            <table className="table s-men"><thead>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar2">110hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar3">50hrs/Week </a>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </thead></table>
                        </div>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <table className="table s-men"><thead>
                            <tr>
                                <td>
                                    <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-blue bar-head">115hrs/week</a>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </thead></table>
                    </div>
                    <div id="collapse4p" className="panel-collapse collapse ">
                        <div className="panel-body">
                            <table className="table s-men"><thead>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar2">110hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-blue bar3">50hrs/Week </a>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-blue bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </thead></table>
                        </div>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <table className="table s-men"><thead>
                            <tr>
                                <td>
                                    <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-pink bar-head">115hrs/week</a>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </thead></table>
                    </div>
                    <div id="collapse5p" className="panel-collapse collapse ">
                        <div className="panel-body">
                            <table className="table s-men"><thead>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-pink bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-pink bar2">110hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-pink bar3">50hrs/Week </a>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-pink bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </thead></table>
                        </div>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <table className="table s-men"><thead>
                            <tr>
                                <td>
                                    <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-grey bar-head">115hrs/week</a>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </thead></table>
                    </div>
                    <div id="collapse6p" className="panel-collapse collapse ">
                        <div className="panel-body">
                            <table className="table s-men"><thead>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar2">110hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar"  className="btn btn-default btn btn-sm btn-success bar-color-grey bar3">50hrs/Week </a>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <a name="poll_bar" className="btn btn-default btn btn-sm btn-success bar-color-grey bar1">115hrs/Week </a>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </thead></table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth,
		errorMessage: state.auth.errorMessage || state.errors.notAuthorized,

		userResult:state.home.userResult,
		userAddResult:state.home.userAddResult,
		submitFlag:state.home.submitFlag,
		Eusername:state.home.Eusername,
		userAddFlag:state.home.userAddFlag,
		formFlag:state.home.formFlag,
		driverList:state.home.driverList,
		driverDetail:state.home.driverDetail,
		exportDriverDetail:state.home.exportDriverDetail,
		exportFlag:state.home.exportFlag,

	
	};
}

export default connect(mapStateToProps)(userView);
