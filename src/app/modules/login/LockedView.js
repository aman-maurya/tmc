import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Form, FormGroup, FormError, Input} from '../../components/forms/Forms';
import { Link } from 'react-router';

class LockedView extends Component {
	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
	}
	onSubmit(data){
		const {pin} = data;

		this.props.dispatch({
			type: 'SUBMIT_UNLOCK',
			payload: {
				pin
			}
		});

	}
	render() {
		let {errorMessage, username, avatar} = this.props;

		return (
			<div className="locked-view">

				<Form onSubmit={this.onSubmit} validateFrequency="onChange" updateFrequency="onSubmit">
					{errorMessage ? (
						<p className="error">
							{errorMessage}
						</p>
					) : null}
					<div className="user">
						<img src={avatar} alt="" width="100"/>
						{username}
					</div>
					<FormGroup>
						<label>Pin</label>
						<Input name="pin" type="password" value="1234" validation={['required']}/>
						<FormError for="required">
							Enter Pin
						</FormError>
					</FormGroup>
					<button>Unlock</button>
				</Form>
			</div>
		);
	}
}

function mapStateToProps(state, ownProps) {

	return {
		...state.auth,
		errorMessage: state.auth.unlockErrorMessage
	}
}

export default connect(mapStateToProps)(LockedView);

