import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Form, FormGroup, FormError, Input} from '../../components/forms/Forms';
import { Link } from 'react-router';

class LoginView extends Component {
	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
	}
	onSubmit(data){
		const {username, password} = data;
		const save = !!data.save;

		this.props.dispatch({
			type: 'SUBMIT_LOGIN',
			payload: {
				username,
				password,
				save
			}
		});

	}
	render() {
		let {auth, errorMessage} = this.props;

		// https://facebook.github.io/react/docs/forms.html
		// todo: check if this re-renders because we changed state will defaultValue be changed?? if not that will solve our issue with profile (according to docs it doesn't)
		return (
			<div className="login-head-div" >

			<div className=" login-view-from back-image container login  col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">

					<div className="text-center login-top first-div col-md-6 col-md-offset-3 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2">
						
							<Form className="form-horizontal" onSubmit={this.onSubmit} className="text-center login-top first-div col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
									<div className="form-group">
											{errorMessage && <p className="error red">{errorMessage}</p>}
											<Input type="text" className="form-control" name="username" id="username" placeholder="Username" value="" validation={[ 'required']}/>
											<FormError for="required">
												Enter username
											</FormError>
									</div>
									<div className="form-group">
											<Input type="password" className="form-control" name="password" id="password" placeholder="Password" value="" validation={[ 'required']}/>
											<FormError for="required">
												Enter password
											</FormError>
									</div>
								<div className="form-group" >
										<button type="submit" className="btn btn-primary buttonGradient" name="login" id="loginValidation">{auth.loginFetching ? 'Loading...' : 'LOGIN'}</button>
								</div>
							</Form>
						
					</div>
			</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth,
		errorMessage: state.auth.errorMessage || state.errors.notAuthorized
	};
}

export default connect(mapStateToProps)(LoginView);
