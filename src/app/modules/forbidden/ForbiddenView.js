import React, {Component} from 'react';
import { Link } from 'react-router';

class HomeView extends Component {

	render() {
		return (
			<div className="forbidden-view">
				<h1>403 Forbidden</h1>
				<p>You do not have access to this resource <Link to="/">Go to home page</Link></p>
			</div>
		);
	}
}

export default HomeView;
