import React, {Component} from 'react';
import ReactDom from 'react-dom';

class RootInject extends Component{
	componentDidMount(){
		this.node = document.createElement('div');
		document.body.appendChild(this.node);
		this.renderManually(this.props);
	}
	componentWillUnmount(){
		ReactDom.unmountComponentAtNode(this.node);
		document.body.removeChild(this.node);
	}
	componentWillReceiveProps(nextProps) {
		this.renderManually(nextProps);
	}

	renderManually(props){
		ReactDom.unstable_renderSubtreeIntoContainer(this, props.children, this.node);
	}
	render(){
		return React.DOM.noscript();
	}
}

export default RootInject;
