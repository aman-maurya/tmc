'use strict';

const rules = {
	numeric:{
		validator: /^[0-9]+$/
	},
	integer:{
		validator: /^\-?[0-9]+$/
	},
	decimal:{
		validator: /^\-?[0-9]*\.?[0-9]+$/
	},
	email:{
		validator: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i,
	},
	alpha:{
		validator: /^[a-z]+$/i
	},
	alphaNumeric:{
		validator: /^[a-z0-9]+$/i
	},
	alphaDash:{
		validator: /^[a-z0-9_\-]+$/i
	},
	natural:{
		validator: /^[0-9]+$/i
	},
	naturalNoZero:{
		validator: /^[1-9][0-9]*$/i
	},
	ip:{
		validator: /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i
	},
	base64:{
		validator: /[^a-zA-Z0-9\/\+=]/i
	},
	numericDash:{
		validator: /^[\d\-\s]+$/
	},
	url:{
		validator: /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/
	},
	phoneUS:{
		validator: /^(1-?)?(\([0-9]\d{2}\)|[0-9]\d{2})[- ]?[0-9]\d{2}[- ]?\d{4}$/
	},
	zip:{
		validator: /(^\d{5}(-\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$)/
	},
	//this is for usa and canada zip codes
	zipUSCA: {
		validator: /(^([0-9]{5})$)|(^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$)/
	},
	required:{
		validator: function(value, data, dependsOn, dependsValue){
			value = ( value || '' ).trim();
			if(dependsOn){
				if((!dependsValue && !data[dependsOn].value && arguments.length<4) || (data[dependsOn].value!==dependsValue)){
					return true;
				}
			}
			return !!value;
		}
	},
	not: {
		validator: function(value, data){
			return Array.prototype.slice.call(arguments, 2).indexOf(value) === -1;
		}
	},
	in:{
		validator: function(value, data){
			return Array.prototype.slice.call(arguments, 2).indexOf(value) > -1;
		}
	},
	between:{
		validator: function(value,data,min,max){
			var l=value.length;
			return (l>=min && l<=max);
		}
	},
	min:{
		validator: function(value,data,min){
			var l=value.length;
			return (l>=min);
		}
	},
	max:{
		validator: function(value,data,max){
			var l=value.length;
			return (l<=max);
		}
	},
	length:{
		validator: function(value,data,length){
			var l=value.length;
			return (l===length);
		}
	},
	cc:{
		validator: function(value){
			// Luhn Check Code from https://gist.github.com/4075533
			// accept only digits, dashes or spaces
			if (!rules.numericDash.validator.test(value)){
				return false;
			}
			var nCheck = 0, nDigit = 0, bEven = false;
			var strippedField = value.replace(/\D/g, "");

			for (var n = strippedField.length - 1; n >= 0; n--) {
				var cDigit = strippedField.charAt(n);
				nDigit = parseInt(cDigit, 10);
				if (bEven) {
					if ((nDigit *= 2) > 9){ nDigit -= 9;}
				}

				nCheck += nDigit;
				bEven = !bEven;
			}

			return (nCheck % 10) === 0;
		}
	},
	match:{
		validator: function(value,data,	field){
			return value === data[field].value;
		}
	}
};

export default rules;


