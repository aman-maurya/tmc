'use strict';
import rules from './Rules';
import forEach from 'lodash/forEach';
import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import assign from 'lodash/assign';
import Bluebird from 'bluebird';

export default class Validator{
	static validate(fields, done){
		const result = {};
		const asyncValidators = [];
		forEach(fields, (state, key)=>{
			if(state.validation && (state.touched || state.value)){
				const errors = [];
				// if the field is empty and not required don't bother validating
				// todo: I need to check if the field is really required in case of dependent required
				if(state.required || state.value){
					forEach(state.validation, (validation)=>{
						// TODO: I could improve performance by checking if a field was changed and depending on the
						// TODO: validation to validate if it was changed (config ith validator). There is an issue
						// TODO: with that though is that if I put an initial value the previous value is undefined
						// TODO: which is what we want the first time round but then we want the previous value to be
						// TODO: set to something subsequentrly, maybe changing the previous if it's undefined after
						// TODO: validating might be the solution.
						if(validation.type==='async'){
							asyncValidators.push({
								validation: validation,
								key: key
							});
							return;
						}
						const rule = rules[validation.type];
						if(!rule){
							throw new Error('Rule "' + validation.type + '" does not exist.');
						}
						if(rule.validator instanceof RegExp){
							if(!state.value.match(rule.validator)){
								errors.push(validation.key);
							}
						}else{
							if(!rule.validator.apply(rule, [state.value, fields].concat(validation.params))){
								errors.push(validation.key);
							}
						}
					});
				}

				result[key] = {
					...state,
					errors: errors.length ? errors : null
				};
				return;
			}
			result[key] = state;
		});

		if(asyncValidators.length){
			// TODO: this code has not been tested
			const asyncErrors = {};
			Bluebird
				.map(asyncValidators, (asyncValidator)=>{
					// I'm just passing the params object for now since I'm not sure it's even needed
					return asyncValidator.validation.params[0](fields[asyncValidator.key], fields, asyncValidator.validation.params)
						.then((result)=>{
							asyncErrors[asyncValidator.key] = asyncErrors[asyncValidator.key] || [];
							if(!result.valid){
								asyncErrors[asyncValidator.key].push(asyncValidator.validation.key);
							}
						});
				})
				.then(()=>{
					var newResult = {
						...fields
					};
					// I need to group them by key or something
					forEach(asyncErrors, (errors, key)=>{
						newResult[key] = {
							...newResult[key],
							errors: errors.length ? errors : null
						};
					});
					done(newResult);
				})
		}

		// although we return the result we can also call done for any async validation that was going on, remember to
		// pass in previous and next values to async validator so that it can skip doing validation if it does not need to
		// i.e. checking for if the username is taken
		//done(result);
		return result;

	}
	static normalize(rules){
		// validation={['email', ['match', 'email'], {type: 'match', params: ['email'], key: 'match1'}]}
		// validation={'email'}
		let _rules;
		let required = false;
		if(isString(rules)){
			_rules = [
				{
					type: rules,
					key: rules,
					params: []
				}
			];
		}else if(isArray(rules)){
			_rules = [];
			rules.forEach((rule)=>{
				let _rule;
				if(isString(rule)){
					_rule = {
						type: rule,
						key: rule,
						params: []
					};
				}else if(isArray(rule)){
					_rule = {
						type: rule[0],
						key: rule[0],
						params: rule.slice(1)
					}
				}else if(isObject(rule)){
					_rule = assign({
						key: rule.type,
						params: []
					}, rule);
				}
				if(_rule.type==='required'){
					required=true;
					_rules.unshift(_rule);
				}else{
					_rules.push(_rule);
				}
			});
		}
		return {
			rules: _rules,
			required: required
		};
	}
}
