/**
 * TODO: add/test defaultValue support
 */
import React, {Component} from 'react';
import fpSet from 'lodash/fp/set';
import Validator from './Validator';
import omit from 'lodash/omit';
import forEach from 'lodash/forEach';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isString';

// todo: I should have a prop validate with options (blur|change|submit) that will specify when to validate
// todo: I should also have a update with the same options (blur|change|submit) (provide this down to inputs so they act accordingly or just skip change/update stuff)
export class Form extends Component {
	constructor(props){
		super(props);
		this.validateId = 0;
		this.formValues = {
			values: {},
			flatValues: {}
		};
		this.onChange = this.onChange.bind(this);
		this.onBlur = this.onBlur.bind(this);
		this.getState = this.getState.bind(this);
		this.register = this.register.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}
	getChildContext() {
		return {
			onChange : this.onChange,
			onBlur : this.onBlur,
			register : this.register,
			formValues: this.formValues,
			getState: this.getState
		};
	}
	componentDidMount(){
		this.validate();
		this.forceUpdate();
	}
	getState(name){
		return	this.formValues.flatValues[name];
	}
	validate(){
		this.validateId++;
		const lastId = this.validateId;


		// the callback will only be called if there was an actual async validator.
		const validatedFlatValues = Validator.validate(this.formValues.flatValues, (asyncValidatedFlatValues)=>{
			// make sure that if we are doing async validation we get only update our state with the latest validation results
			if(lastId===this.validateId){
				this.formValues = {
					...this.formValues,
					flatValues: asyncValidatedFlatValues
				};
				this.forceUpdate();
			}
		});
		this.formValues = {
			...this.formValues,
			flatValues: validatedFlatValues
		};


	}
	handleChange({name, value, source}){
		this.formValues = {
			values: fpSet(name, value, this.formValues.values),
			flatValues: {
				...this.formValues.flatValues,
				[name]: {
					...this.formValues.flatValues[name],
					value: value,
					touched: true,
					previousValue: this.formValues.flatValues[name].value
				}
			}
		};

		this.propagateChange(source);
	}
	propagateChange(source){
		// onChange|onBlur|onSubmit|change|blur|submit
		if((this.props.validateFrequency || 'onChange') === source ){
			this.validate();
		}
		// this.dataChanged makes sure on blur does not fire when there was never any change
		if(this.dataChanged && this.props.onUpdate && (this.props.updateFrequency || 'onChange') === source){
			this.dataChanged = false;
			this.props.onUpdate(this.formValues.values, this.formValues.flatValues);
		}
		this.forceUpdate();


	}

	onChange(options) {
		this.dataChanged = true;
		return this.handleChange({
			...options,
			source: 'onChange'
		});
	}
	onBlur(options) {
		return this.handleChange({
			...options,
			source: 'onBlur'
		});
	}
	onSubmit(e){
		e.preventDefault();
		// TODO: change all inputs to touched and then validate
		const flatValues = {};
		forEach(this.formValues.flatValues, (state, key)=>{
			flatValues[key] = {
				...state,
				touched: true
			}
		});
		this.formValues = {
			...this.formValues,
			flatValues
		};
		this.propagateChange('onSubmit');
		if(this.props.onSubmit){
			return this.props.onSubmit( this.formValues.values, this.formValues.flatValues, e);
		}
	}
	register({name, value, props, skipRegisterIfExists}){
		if(this.formValues.flatValues[name] && skipRegisterIfExists){
			return;
		}
		const normalizedValidation = Validator.normalize(props['validation']);
		this.formValues = {
			values: fpSet(name, value, this.formValues.values),
			flatValues: {
				...this.formValues.flatValues,
				[name]: {
					value: value,
					props: props,
					validation: normalizedValidation.rules,
					required: normalizedValidation.required
				}
			}
		};
	}

	render() {
		return (
			<form action="" className={this.props.className || ''} onSubmit={this.onSubmit}>
				{this.props.children}
			</form>
		);
	}
}

Form.childContextTypes = {
	onChange: React.PropTypes.func,
	onBlur: React.PropTypes.func,
	register: React.PropTypes.func,
	getState: React.PropTypes.func,
	formValues: React.PropTypes.object
};



export class FormGroup extends Component{
	constructor(props){
		super(props);
		this.onChange = this.onChange.bind(this);
		this.onBlur = this.onBlur.bind(this);
		this.register = this.register.bind(this);
	}
	getChildContext() {
		return {
			onChange : this.onChange,
			onBlur : this.onBlur,
			register : this.register,
			formValues: this.context.formValues,
			inputState: this.context.formValues.flatValues[this.name]
		};
	}
	onChange(options){
		return this.context.onChange(options);
	}
	onBlur(options){
		return this.context.onBlur(options);
	}
	register(options){
		const {name} = options;
		this.name = name;
		return this.context.register(options);
	}
	render(){
		const state = this.context.formValues.flatValues[this.name];
		if(state){
			const classes = ['input'];
			if(this.props.className){
				classes.push(this.props.className);
			}
			if(state.errors){
				classes.push('has-error');
				state.errors.forEach((error)=>{
					classes.push('has-error-' + error);
				});
			}
			return (
				<div className={classes.join(' ')}>
					{this.props.children}
				</div>
			)
		}
		// this simple version is here just so that the children can register themselves and we can get state
		return (
			<div className="input">
				{this.props.children}
			</div>
		);

	}
}
FormGroup.contextTypes = {
	onBlur: React.PropTypes.func,
	onChange: React.PropTypes.func,
	register: React.PropTypes.func,
	formValues: React.PropTypes.object
};
FormGroup.childContextTypes = {
	onBlur: React.PropTypes.func,
	onChange: React.PropTypes.func,
	register: React.PropTypes.func,
	formValues: React.PropTypes.object,
	inputState: React.PropTypes.object
};

class BaseInput extends Component{
	constructor(props){
		super(props);
		this.onChange = this.onChange.bind(this);
		this.onBlur = this.onBlur.bind(this);
	}
	componentWillMount(){
		this.context.register({
			name: this.props.name,
			value: (this.props.type === 'checkbox' && !this.props.checked) ? undefined : this.props.value,
			props: this.props,
			skipRegisterIfExists: this.props.type === 'radio' && !this.props.checked
		});
	}
	shouldComponentUpdate(nextProps, nextState, nextContext){
		if(nextProps.value !== this.props.value){
			this.context.onChange({
				name: this.props.name,
				value: nextProps.value
			});
			return false;
		}
		// only render if the value has changed we will also want to render if there are
		if(this.context.formValues.flatValues[this.props.name] !== nextContext.formValues.flatValues[this.props.name]){
			return true;
		}
		return false;

	}
	onChange(e){
		const value = (this.props.type === 'checkbox' && !e.target.checked) ? undefined : e.target.value;
		this.context.onChange({
			name: this.props.name,
			value: value
		});
		if(this.props.type === 'checkbox' || this.props.type === 'radio' || this.inputType === 'select'){
			// for checkboxes we want to fire onBlur udpates when onChange happens
			this.context.onBlur({
				name: this.props.name,
				value: value
			});
		}
		if(this.props.onChange){
			return this.props.onChange(e);
		}
	}
	onBlur(e){
		// the onChange is handling this for checkbox and maybe the others so we don't run this in those cases
		if(['checkbox','radio','select'].indexOf(this.props.type) === -1){
			this.context.onBlur({
				name: this.props.name,
				value: e.target.value
			});
			if(this.props.onBlur){
				return this.props.onBlur(e);
			}
		}

	}
	getClasses(extraClasses, state){
		let classes = [];

		if(this.props.className){
			classes.push(this.props.className);
		}
		if(state.errors){
			classes.push('has-error');
			state.errors.forEach((error)=>{
				classes.push('has-error-' + error);
			});
		}
		if(extraClasses){
			classes = classes.concat(extraClasses);
		}
		return classes;
	}
}

export class Input extends BaseInput {
	constructor(props){
		super(props);
		this.inputType = 'input';
	}
	render() {
		const state = this.context.formValues.flatValues[this.props.name];
		if(!state){
			return null;
		}
		// const value = state.value || '';
		let checked = undefined;
		let value = state.value || '';
		if(this.props.type === 'radio' || this.props.type === 'checkbox'){
			value = this.props.value;
			checked = state.value === this.props.value;
		}
		return (
			<input
				{...omit(this.props, 'validation', 'checked')}
				className={this.getClasses([],state).join(' ')}
				type={this.props.type || 'text'}
				value={value}
				checked={checked}
				onChange={this.onChange}
				onBlur={this.onBlur}/>
		);
	}
}
Input.contextTypes = {
	onBlur: React.PropTypes.func,
	onChange: React.PropTypes.func,
	register: React.PropTypes.func,
	formValues: React.PropTypes.object
};

export class Textarea extends BaseInput {
	constructor(props){
		super(props);
		this.inputType = 'textarea';
	}
	render() {
		const state = this.context.formValues.flatValues[this.props.name];
		if(!state){
			return null;
		}
		return (
			<textarea
				{...omit(this.props, 'validation')}
				className={this.getClasses([],state).join(' ')}
				value={state.value || ''}
				onChange={this.onChange}
				onBlur={this.onBlur}/>
		);
	}
}
Textarea.contextTypes = {
	onBlur: React.PropTypes.func,
	onChange: React.PropTypes.func,
	register: React.PropTypes.func,
	formValues: React.PropTypes.object
};

export class Select extends BaseInput {
	constructor(props){
		super(props);
		this.inputType = 'select';
	}
	render() {
		const {name, options} = this.props;
		const state = this.context.formValues.flatValues[name];
		if(!state){
			return null;
		}
		let elementOptions = this.props.children;
		if(options){
			let formattedOptions = options;
			if(isArray(options) && typeof options[0] !== 'object'){
				formattedOptions = options.map((value)=>{
					return {
						key: value,
						value: value
					}
				});
			}else if(typeof options[0] !== 'object'){
				formattedOptions = [];
				forEach(options, (value, key)=>{
					formattedOptions.push({
						key,
						value
					});
				});
			}
			elementOptions = (formattedOptions.map((option, i)=>{
				return (<option key={'select_option_' + name + '_' + i} value={formattedOptions.key}>{option.value}</option>)
			}));

		}
		return (
			<select
				{...omit(this.props, 'validation', 'options')}
				className={this.getClasses([],state).join(' ')}
				value={state.value || ''}
				onChange={this.onChange}>
				{elementOptions}
			</select>
		);
	}
}
Select.contextTypes = {
	onBlur: React.PropTypes.func,
	onChange: React.PropTypes.func,
	register: React.PropTypes.func,
	formValues: React.PropTypes.object
};

export class FormError extends Component{
	shouldComponentUpdate(nextProps, nextState, nextContext){
		// only render if the value has changed we will also want to render if there are
		if(this.context.inputState !== nextContext.inputState){
			return true;
		}
		return false;

	}
	render(){
		// until we get inputState there is no reason to render anything
		const state = this.context.inputState;
		if(!state){
			return null;
		}
		const classes = ['forms-validation-error'];
		if(this.props.className){
			classes.push(this.props.className);
		}

		if(this.props.for){
			classes.push('forms-validation-error-' + this.props.for);
		}
		if(state.errors){
			// todo: I'm checking if the first error matches the for, I could make for === to array for multimatching
			// regardless if I want something very custom I can always override with css
			if(!this.props.for || state.errors[0] === this.props.for /*state.errors.indexOf(this.props.for) > -1*/){
				classes.push('active');
			}
		}
		return (
			<div className={classes.join(' ')}>
				{this.props.children || 'Validation error'}
			</div>
		)
	}
}
FormError.contextTypes = {
	onChange: React.PropTypes.func,
	register: React.PropTypes.func,
	formValues: React.PropTypes.object,
	inputState: React.PropTypes.object
};
