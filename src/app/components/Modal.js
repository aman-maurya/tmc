import React, {Component} from 'react';
import RootInject from './RootInject';

class Modal extends Component{
    render(){
        const { className, open, showHeader, onClose } = this.props;
        return (
            <RootInject>
                <div>
                    {open  ? (
                        <div className={'modal-container' + (className ? ' ' + className : '')}>
                            <div className="modal-content">
                                {this.props.children}
                            </div>
                        </div>
                    ) : null}
                </div>
            </RootInject>
        );
    }
}

export default Modal;
