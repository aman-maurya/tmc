import React, {Component} from 'react';
import { connect } from 'react-redux';

class LoaderLayoutView extends Component {

	render() {
		let {loader} = this.props;

		return loader ? (
			<div className="main-loader">
				Loading ...
			</div>
		) : this.props.children;
	}
}

function mapStateToProps(state, ownProps) {

	return {
		loader: 0
	}
}

export default connect(mapStateToProps)(LoaderLayoutView);
