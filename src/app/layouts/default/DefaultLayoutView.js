import './defaultLayout.less';

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';

class DefaultLayoutView extends Component {

	matchLocation() {
		switch (this.props.location.pathname) {
			case '/':
				return 'Home';
		}

		return '';
	}
		componentWillMount() {
		return this.getData();
	}

	getData() {
		this.props.dispatch({
			type: 'FETCH_LAYOUT_DATA'
		});
	}
	logOut(){
		event.preventDefault;
		const {dispatch} = this.props;
		dispatch({
			type: 'LOGOUT_ADMIN'
		});
	}
	navigateTo(link,event)
	{
				location.href=link;

	}
	render () {
		let {auth,location} = this.props;
		return (
				<div className="default-layout">
					    <div >
					       <div className="menu">
							    <div className="container-fluid">
							        <div className="navbar-header">
							            <a href="#">HR Projects</a>
							        </div>
							        <div>
							            <ul className="nav navbar-nav navbar-right">
							                <li><a href="#"><span className="glyphicon glyphicon-share"></span>Share</a></li>
							                <li><a href="#"><span className="glyphicon glyphicon-pencil"></span>Edit</a></li>
							                <li><a href="#"><span className="glyphicon glyphicon-trash"></span>Delete</a></li>
							                <li><a href="#"><span className="glyphicon glyphicon-floppy-disk"></span>Save</a></li>
							            </ul>
							        </div>
							    </div>
							</div>
					    </div>
						    <div className="content">
						        {this.props.children}
						    </div>
						    <footer>
						    </footer>				</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		...state,
		auth: state.auth
	}
}

export default connect(mapStateToProps)(DefaultLayoutView);
