'use strict';
import isString from 'lodash/isString';
import assign from 'lodash/assign';
import isObject from 'lodash/isObject';
import forEach from 'lodash/forEach';
import config from '../../../config';
import Auth from './Auth';
import {HttpNotFoundError, HttpNotAuthorizedError, HttpForbiddenError, HttpServerError} from '../errors';

// const API_URL = 'http://localhost:3333/api/';
// const API_URL = 'https://wby8h7j93m.execute-api.us-west-2.amazonaws.com/dev/';

const API_URL = config.apiUrl;

class Http{
	static get(url, options){
		return _callRequest('GET', url, options);
	}
	static post(url, options){
		return _callRequest('POST', url, options);
	}
	static put(url, options){
		return _callRequest('PUT', url, options);
	}
	static delete(url, options){
		return _callRequest('DELETE', url, options);
	}
	static request(options){
		let url = options.url;
		const qs = options.query || options.qs;
		if (qs) {
			var parts = [];
			forEach(qs, (val, key)=> {
				parts.push(key + '=' + encodeURIComponent(val));
			});
			if (parts.length) {
				url += url.indexOf('?') === -1 ? '?' : '&';
				url += parts.join('&');
			}
		}

		const headers = {
			...options.headers,
			'auth-token': Auth.getToken() || ''
		};
		return fetch(url, {
			method: options.method || (options.body ? 'POST' : 'GET'),
			headers: (()=>{
				// unless the body is a string we assume that we are doing a JSON request
				if(options.body && isString(options.body)){
					return headers;
				}else{
					return assign({
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					}, headers);
				}
			})(),
			body: (()=>{
				if(options.body && isObject(options.body)){
					return JSON.stringify(options.body);
				}
				return options.body;
			})(),
			// credentials: options.credentials || 'include' // same-origin (sets if cookies should be sent, include will do cross origin)
		})
			.then((response)=>{
				if (response.status >= 200 && response.status < 300) {
					// todo: not taking non json responses into consideration
					return response.json();
				} else {
					let error;
					if (response.status === 500) {
						error = new HttpServerError(/*response.statusText*/);
					} else if (response.status === 401) {
						error = new HttpNotAuthorizedError(/*response.statusText*/);
					} else if (response.status === 403) {
						error = new HttpForbiddenError(/*response.statusText*/);
					} else if (response.status === 404) {
						error = new HttpNotFoundError(/*response.statusText*/);
					} else {
						error = new Error(response.statusText);
					}
					error.response = response;
					throw error
				}
			});
	}
	static rpc(remoteMethod, data){
		data.token = Auth.getToken() || '';
		return Http.request({
			url: API_URL + remoteMethod,
			method: 'POST',
			body: data
		})
			.then((result)=>{
				const statusCode = result.statusCode;
				let error;
				if (statusCode === 500) {
					error = new HttpServerError(result.errorMessage);
				} else if (statusCode === 401) {
					error = new HttpNotAuthorizedError(result.errorMessage);
				} else if (statusCode === 403) {
					error = new HttpForbiddenError(result.errorMessage);
				} else if (statusCode === 404) {
					error = new HttpNotFoundError(result.errorMessage);
				}
				if(error){
					error.response = result;
					throw error
				}
				return result;

			});
	}
}

function _callRequest(method, url, options){
	if(isString(url)){
		return Http.request({
			...options,
			url,
			method
		});
	}
	return Http.request({
		url,
		method
	});
}

export default Http;
