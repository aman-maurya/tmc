'use strict';

import {NotAuthorizedError} from '../errors';

class Auth{
	static isAuthorized({permission, dispatch}){
		// todo: if no token then notAuthorized
		// todo: if token but no access then forbidden (not doing this for boilerplate
		let authData = Auth.getAuthData();
		if(authData.token){
			return authData.role;
		}
		if(dispatch){
			dispatch({
				type: 'AUTH_ERROR',
				payload: new NotAuthorizedError(),
				error: true
			});
		}
		return false;
	}
	static getAuthData(){
		const authString = localStorage.getItem('auth');
		const locked = localStorage.getItem('locked') === 'true';

		let data = {
			locked
		};
		if(authString){
			Object.assign(data, JSON.parse(authString));
		}
		return data;
	}
	static setAuthData(data){
		localStorage.setItem('auth', JSON.stringify(data));
	}
	static lock(){
		localStorage.setItem('locked', 'true');
	}
	static unLock(pin){
		const data = Auth.getAuthData();
		if(pin === data.pin){
			localStorage.setItem('locked', 'false');
			return true;
		}
		return false;

	}
	static getToken(){
		const data = Auth.getAuthData();
		return data.token;
	}
}

export default Auth;
