'use strict';
import { call, put, apply } from 'redux-saga/effects';

/**
 * This saga runs every minute and is used to dispatch other events that need to happen periodically
 * @param action
 */
export function* updateInterval(action){
	const meta = {
		source: 'interval'
	};
}

export default function*({takeEvery, takeFirst, getState}){
	yield [
		takeFirst('UPDATE_INTERVAL', updateInterval)
	];
}
