'use strict';
import { call, put, apply } from 'redux-saga/effects';
import config from 'config';
import {hashHistory} from 'react-router';
import Http from '../../libs/Http';
import Auth from '../../libs/Auth';
import {NotAuthorizedError} from '../../errors';

export function* submitLogin(action){
	const {payload, meta} = action;

	const data = yield call(Http.rpc, 'users', {
		action: 'login',
		body: {
			username: payload.username,
			password: payload.password,
			save: payload.save
		}
	});

	if(data.role!='driver'){
	yield put({
		type: 'LOGIN',
		payload: data,
		meta
	});
			yield call(Auth.setAuthData, data);
			if(data.role=='clientadmin')
			{
						yield apply(hashHistory, hashHistory.push, ['/user']);

			}
						else if(data.role=='truckfitadmin')
			{
						yield apply(hashHistory, hashHistory.push, ['/client']);

			}
						else if(data.role=='clientexecutive')
			{
						yield apply(hashHistory, hashHistory.push, ['/visualization']);

			}
						else if(data.role=='truckfitnutritionexpert')
			{
						yield apply(hashHistory, hashHistory.push, ['/assigndriver']);

			}
			else
			{
				yield apply(hashHistory, hashHistory.push, ['/']);

			}

	}
	else
	{
		yield put({
		type: 'AUTH_ERROR',
				payload: new NotAuthorizedError(),
				error: true,
		meta
	});
	}
	// yield apply(hashHistory, hashHistory.push, ['/']);

}

export function* logOutAdmin(action){
	const {payload, meta} = action;
	let data = null;
	//yield call(Auth.setAuthData, data);
	let aa = localStorage.removeItem('auth');
	yield apply(hashHistory, hashHistory.push, ['/home']);
		yield put({
		type: 'CLEAR_SESSION',
		payload,
		meta
	});

}

export default function*({takeEvery, takeFirst, getState}){
	yield [
		takeFirst('SUBMIT_LOGIN', submitLogin),
		takeFirst('LOGOUT_ADMIN', logOutAdmin)
	];
}
