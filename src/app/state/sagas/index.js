'use strict';
import { takeEvery, takeLatest, delay} from 'redux-saga';
import { call, put, apply, take, fork  } from 'redux-saga/effects';
import {hashHistory} from 'react-router';

import appSagas from './appSagas';
import authSagas from './authSagas';
import homeSagas from './homeSagas';


export default (getState) => function*() {
	const options = {
		// takeEvery: takeEveryAndWrapError,
		takeEvery: wrap(takeEvery),
		takeLatest: wrap(takeLatest),
		takeFirst: wrap(takeFirst),
		getState: getState
	};
	yield [
		appSagas(options),
		authSagas(options),
		homeSagas(options)
	];
}

// function* takeFirst(pattern, saga, ...args) {
//
// 	const task = yield fork(function* () {
// 		while (true) {
// 			const action = yield take(pattern)
// 			yield fork(saga, ...args.concat(action))
// 		}
// 	});
// 	return task
// }
function *takeFirst(pattern, saga, ...args){
	return  yield fork(function* () {
		while(true){
			const action = yield take(pattern);
			yield saga.call({}, ...args.concat(action));
		}
	})

}
/*
function* takeFirst(pattern, saga, ...args) {

	const task = yield fork(function* () {
		while (true) {
			const action = yield take(pattern)
			yield fork(saga, ...args.concat(action))
		}
	});
	return task
}*/



function wrap(func){
	return function(){
		var args = new Array(arguments.length);
		for(var i = 0; i < args.length; ++i) {
			args[i] = i === 1 ? wrapError(arguments[i]) : arguments[i];
		}
		return func.apply(this, args);
	}
}

function wrapError(method){
	return function*(...args){
		try{
			yield method(...args);
		}catch(error){
			console.error && console.error(error.stack);
			// we dispatch an event with the error
			yield put({
				type: 'ERROR',
				error: true,
				payload: error
			});
			// make sure to redirect if we get an 401 || 403 error
			if(error.name === 'HttpNotAuthorizedError'){
				yield apply(hashHistory, hashHistory.push, ['/login']);
			}else if(error.name === 'HttpForbiddenError'){
				yield apply(hashHistory, hashHistory.push, ['/login']);
			}
		}

	};
}
