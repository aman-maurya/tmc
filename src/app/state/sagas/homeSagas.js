'use strict';
import { call, fork, put, apply, join, take } from 'redux-saga/effects';
import Http from '../../libs/Http';

export function* fetchHomeData(action){
	const {payload, meta} = action;
	yield put({
		type: 'FETCH_USER_DETAILS',
		payload,
		meta
	});
	yield put({
		type: 'FETCH_COMPANY_DATA',
		payload,
		meta
	});

}

export function* checkUsername(action){

	const {payload, meta} = action;
	yield put({
		type: 'USERNAME_CHECKING',
		payload,
		meta
	});


	const data = yield call(Http.rpc, 'signup', {
		action: 'checkUsername',
		body: payload
	});

	yield put({
		type: 'USERNAME_CHECKED',
		payload: data,
		meta
	});
}

export default function*({takeEvery, takeFirst, getState}){
	yield [
		takeFirst('FETCH_HOME_DATA', fetchHomeData),
		takeFirst('USERNAME_CHECK', checkUsername)
	];

}
