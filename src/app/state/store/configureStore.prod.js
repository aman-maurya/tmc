import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducers'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas'

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(preloadedState) {
	const store = createStore(
		rootReducer,
		preloadedState,
		applyMiddleware(thunk, sagaMiddleware)
	);
	sagaMiddleware.run(rootSaga(store.getState));
	return store;
}
