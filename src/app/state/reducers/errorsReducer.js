'use strict';
import omitBy from 'lodash/omitBy'

const initialState = {};

export default function (state = initialState, action) {
	if(action.error){
		switch (action.payload.name){
			case 'HttpNotAuthorizedError':
			case 'NotAuthorizedError':
				return {
					...state,
					notAuthorized: action.payload.message,
					persist: {
						...state.persist,
						notAuthorized: 'once'
					}
				};
			case 'HttpForbiddenError':
			case 'ForbiddenError':
				return {
					...state,
					forbidden: action.payload.message,
					persist: {
						...state.persist,
						forbidden: 'once'
					}
				};
			case 'HttpServerError':
			case 'HttpNotFoundError':
				return {
					...state,
					main: action.payload.message
				};
			default:
				return {
					...state,
					main: action.payload.message
				};
		}
	}else{
		switch (action.type) {
			case 'ROUTE': {
				return {
					...omitBy(state, (val, key)=>!state.persist[key]),
					persist: {}
				};
			}
			default:
				return state;
		}
	}

}
