const initialState = {
	addCompanyPopUp:false,
	companyData:[],
	detail: null,
	submitFlag:1,
	Eusername:'',
	userAddFlag:0,
	formFlag:0,
	driverDetail:null,
	exportFlag:0
};

export default function (state = initialState, action) {
	switch (action.type) {
		case 'USERNAME_CHECKED': {
			return {
				...state,
				userResult:action.payload,
				submitFlag:(action.payload.flag==0) ?1 :0,
				Eusername:(action.payload.flag==0) ?'' :' User Name already exists',
				signflag:(action.payload.flag==0) ?1 :0
			}
		}
		case 'USERNAME_ENVALID': {
			return {
				...state,
				Eusername:'Invalid Email',
				submitFlag:0
			}
		}
		case 'USERNAME_CHECKING': {
			return {
				...state,
				
				submitFlag:0,
				Eusername:'Checking for Username'
			}
		}
		case 'USERNAME_REQUIRED': {
			return {
				...state,
				Eusername:'Required'
			}
		}
		default:
			return state;
	}
}
