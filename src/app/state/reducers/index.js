import { combineReducers } from 'redux';

// Import the reducers
import auth from './authReducer';
import home from './homeReducer';
import errors from './errorsReducer';

const rootReducer = combineReducers({
	auth,
	home,
	errors
});

export default rootReducer;
