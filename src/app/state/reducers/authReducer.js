import Auth from '../../libs/Auth';

const initialState = {
	lastInteraction: Date.now(),
	...Auth.getAuthData()
};

export default function (state = initialState, action) {
	switch (action.type) {
		case 'UPDATE_LAST_INTERACTION': {
			return {
				...state,
				lastInteraction: Date.now()
			}
		}
		case 'LOCK': {
			return {
				...state,
				locked: true
			}
		}
		case 'UNLOCK': {
			return {
				...state,
				locked: false
			}
		}
		case 'UNLOCK_ERROR': {
			return {
				...state,
				unlockErrorMessage: action.payload.message
			}

		}
		case 'CLEAR_SESSION': {
			state.role=null;
			return {
				...state
			}

		}
		case 'ROUTE': {
			if(action.payload.prevPage==='login'){
				return {
					...state,
					loginFetching: false,
					errorMessage:''
				};
			}
			return state;

		}
		case 'SUBMIT_LOGIN': {
			return {
				...state,
				loginFetching: true,
				token:'',
				errorMessage:''
			};
		}
		case 'LOGIN': {
			return {
				...state,
				loginFetching: false,
				...action.payload,
				errorMessage:''
			};
		}
		case 'LOGIN_ERROR':
		case 'HTTP_NOT_AUTHORIZED_ERROR':
		{
			return {
				...state,
				loginFetching: false,
				errorMessage: action.errorMessage
			};
		}

		default:
			return state;
	}
}
