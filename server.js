'use strict';

const express = require('express'),
	bodyParser = require('body-parser'),
	fs = require('fs'),
	webpackDevMiddleware = require("webpack-dev-middleware"),
	webpack = require("webpack");

const	webpackConfig = require('./webpack.config.development'),
	pkg = require('./package.json');

const app = express(),
	compiler = webpack(webpackConfig);



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(webpackDevMiddleware(compiler, {
	noInfo: false, // display no info to console (only warnings and errors)
	quiet: false, // display nothing to the console
	lazy: false, // switch into lazy mode, that means no watching, but recompilation on every request
	// watch options (only lazy: false)
	watchOptions: {
		aggregateTimeout: 300,
		poll: true
	},
	publicPath: webpackConfig.output.publicPath, // public path to bind the middleware to, use the same as in webpack
	headers: { "X-Webpack-Dev-Server": "true" }, // custom headers
	stats: {
		colors: true // options for formating the statistics
	}

}));
app.use(require("webpack-hot-middleware")(compiler));

app.use(express.static('./src'));

app.get('*', function(req, res){
	fs.readFile( './src/index.html', function(error, file){
		if(error){
			return res.end(error.stack);
		}
		res.end(file.toString())
	});
});

app.listen(3001, function () {
	console.log(pkg.name + ' listening on port 3001!');
});
