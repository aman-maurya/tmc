var username = $('#username');
var password = $('#password');
var errorUsername = $('.error-username');
var errorPassword = $('.error-password');
$( "#loginValidation" ).click(function() {
	resetError();
	var error=false;
	if(username.val() === "") {
		error=true;
		errorUsername.text('Enter username');
	}
	if(password.val() === "") {
		error=true;
		errorPassword.text('Enter password');
	}
	if(error === false) {
		resetError();
		resetVal();
		alert('successfully login');
	}
});
function resetError() {
	errorUsername.text('');
	errorPassword.text('');
}
function resetVal() {
	username.val("");
	password.val("");
}