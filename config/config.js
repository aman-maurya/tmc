export default {
	apiUrl: 'http://localhost:3333/api/v100/',
	auth: {},
	updateInterval: 5 * 60 * 1000,
	fitbit: {
		state: 'app'
	}
};
