'use strict';

export default {
	apiUrl: 'https://truckfit.herokuapp.com/api/v100/',
	fitbit: {
		state: 'dev'
	}
};
